<?
	$this->beginPage();
	use app\helpers\Debug;
	use app\widgets\TemperatureSwitcher;
	use yii\helpers\Html;
?>

<!DOCTYPE html>

<html lang="ru">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="<?= Yii::getAlias('@web') ?>/css/font-awesome.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="<?= Yii::getAlias('@web') ?>/css/style.css?<?= \app\controllers\FrontendController::VERSION ?>" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<title>Talan test</title>
		<?= Html::csrfMetaTags() ?>
	</head>

	<body class="page">
		<? $this->beginBody(); ?>
		<header class="header">
			<div class="header__wrapper">
				<div class="header__row">
					<div class="header__logo">
						<a class="logo" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>">

						</a>
					</div>
					<div class="header__menu">
						<nav class="menu menu_header">
							<?= \yii\widgets\Menu::widget([
								'items'        => Yii::$app->controller->topMenu,
								'options'      => [
									'class' => 'menu__list',
								],
								'itemOptions'  => ['class' => 'menu__item'],
								'linkTemplate' => '<a class="menu__link" href="{url}">{label}</a>',
							]) ?>
						</nav>
					</div>
					<div class="temperature-switcher">
						<?= app\widgets\TemperatureSwitcher::widget() ?>
					</div>
				</div>
			</div>
		</header>

		<main>
			<?= $content ?>
		</main>

		<footer class="footer"></footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/app.js?<?= \app\controllers\FrontendController::VERSION ?>"></script>
		<? $this->endBody(); ?>
	</body>


</html>
<? $this->endPage(); ?>