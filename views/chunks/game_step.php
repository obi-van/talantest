<?
	use app\helpers\Debug;
?>

<?// echo Debug::vars(); die; ?>

<? if(!empty($cities)): ?>
	<div class="game-step-wrap js--game-step-wrap">
		<div class="message-block js--message-block">
			<?= \app\helpers\Useful::stepMessage($gameInfo) ?>
			<? if($game->status == 'over'): ?>
				<div class="game-compleat">Игра окончена</div>
			<? endif ?>
		</div>
		<? if($game->status == 'on'): ?>
			<div class="game-cities-wrap js--game-cities-wrap">
				<? foreach($cities as $city): ?>
					<span class="radio radio_city js--radio_city<?= isset($gameInfo->city_answer_id) ? ' disabled' : '' ?>">
						<input type="radio" name="city" id="city_<?= $city->id ?>" value="<?= $city->id ?>" class="radio__input" <?= isset($gameInfo->city_answer_id) && $gameInfo->city_answer_id == $city->id ? 'checked' : '' ?> />
						<label for="city_<?= $city->id ?>" class="radio__label"><?= $city->title ?> (<?= $city->country->title ?>)</label>
					</span>
				<? endforeach ?>
			</div>
		<? endif ?>
	</div>
<? endif ?>