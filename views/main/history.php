<section class="section section_no-paddin">
	<div class="container">
		<h1>История игр</h1>
		<table class="table">
			<thead class="thead-light">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Дата</th>
					<th scope="col">Кол-во баллов</th>
					<th scope="col"><i class="fa fa-eye" aria-hidden="true"></i></th>
				</tr>
			</thead>
			<? if(!empty($games)): ?>
				<tbody>
					<? foreach($games as $game): ?>
						<tr style="text-align: center">
							<th scope="row"><?= $game->id ?></th>
							<td><?= $game->create ?></td>
							<td>
								<?= \app\helpers\Useful::quantityPoints($game) ?>
							</td>
							<td>
								<a href="<?= Yii::getAlias('@web') ?>/more/<?= $game->id ?>" class="view-details-link">
									<i class="fa fa-plus" aria-hidden="true"></i>
								</a>
							</td>
						</tr>
					<? endforeach ?>
				</tbody>
			<? endif ?>
		</table>
	</div>
</section>