<?
	use app\helpers\Debug;
?>

<section class="section section_no-paddin">
	<div class="container">
		<h1>Игра</h1>
		<div class="points">
			<span>Набрано очков: <span class="js--points-quantity"><?= \app\helpers\Useful::quantityPoints($game) ?></span></span>
		</div>
		<div class="game-block">
			<?// echo Debug::vars($gameId); die; ?>
			<? if(!isset($game->id)): ?>
				<div class="game-block-action js--game-block-action" data-game-id="new">
					<a href="#" class="btn btn-primary js--game-start">Начать игру</a>
				</div>
				<div class="next-btn-wrap js--next-btn-wrap<?= isset($game->status) && $game->status == 'over' || !isset($gameInfo->city_answer_id) ? ' hidden' : '' ?>">
					<a class="btn btn-success js--next-btn" href="#">Следующие города</a>
				</div>
				<div class="compleat-btn-wrap js--compleat-btn-wrap hidden">
					<a class="btn btn-success js--compleat-btn" href="#">Ок</a>
				</div>
			<? else: ?>
				<div class="game-block-action js--game-block-action" data-game-id="<?= $game->id ?>">
					<?= $this->render('/chunks/game_step', ['game' => $game, 'cities' => $cities, 'gameInfo' => $gameInfo]) ?>
				</div>
				<div class="next-btn-wrap js--next-btn-wrap<?= $game->status == 'over' || !isset($gameInfo->city_answer_id) ? ' hidden' : '' ?>">
					<a class="btn btn-success js--next-btn" href="#">Следующие города</a>
				</div>
				<div class="compleat-btn-wrap js--compleat-btn-wrap<?= $game->status == 'on' ? ' hidden' : '' ?>">
					<a class="btn btn-success js--compleat-btn" href="#">Ок</a>
				</div>
			<? endif ?>
		</div>
	</div>
</section>