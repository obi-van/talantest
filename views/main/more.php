<section class="section section_no-paddin">
	<div class="container">
		<h1>История игры <?= $answers[0]->game_id ?></h1>
		<table class="table">
			<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Город 1</th>
				<th scope="col">Город 2</th>
				<th scope="col">Ваш ответ</th>
				<th scope="col">Статус ответа</th>
			</tr>
			</thead>
			<? if(!empty($answers)): ?>
				<tbody>
				<? foreach($answers as $answer): ?>
					<tr style="text-align: center">
						<th scope="row"><?= $answer->id ?></th>
						<td>
							<?= $answer->cityFirst->title ?>
							<div class="more-temperature-info">
								<?= \app\helpers\Useful::convertToTemperature($answer->cityFirst->celsius) ?>
							</div>
						</td>
						<td>
							<?= $answer->citySecond->title ?>
							<div class="more-temperature-info">
								<?= \app\helpers\Useful::convertToTemperature($answer->citySecond->celsius) ?>
							</div>
						</td>
						<td><?= $answer->cityAnswer->title ?></td>
						<td>
							<? if($answer->responseStatus): ?>
								<i class="fa fa-check" aria-hidden="true"></i>
							<? else: ?>
								<strong>&mdash;</strong>
							<? endif ?>
						</td>
					</tr>
				<? endforeach ?>
				</tbody>
			<? endif ?>
		</table>
	</div>
</section>