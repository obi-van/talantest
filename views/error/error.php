<?
	use yii\helpers\Html;
?>
<div class="page-error page-error_absolute">
	<div class="page-error__info">
		<div class="page-error__code"><?= $code ?></div>
		<p><?= Html::decode($message); ?></p>
	</div>
</div>
