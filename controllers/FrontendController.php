<?php

namespace app\controllers;

use app\models\Temperature;
use Yii;
use yii\web\Controller;
use app\helpers\Debug;
use app\helpers\Arr;
use yii\helpers\Html;

class FrontendController extends Controller
{
	/**
	 * Version
	 */
	const VERSION = 0.02;

	/*
	 * default current temperature
	 */
	const TEMPERATURE_CODE_DEFAULT = 'celsius';

	/**
	 * @var string temperature
	 */
	public $temperatureCode;
	public $temperature;
	public $temperatures = [];

	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = 'main';

	public function beforeAction($action)
	{
		//отключил CSRF ToDo включить и настроить при необходимости
		$this->enableCsrfValidation = false;

		if(!session_id())
		{
			session_start();
		}

		$session = Yii::$app->session;

		if($temperatureSwitcherForm = Arr::get($_POST, 'TemperatureSwitcherForm'))
			$session['temperatureCode'] = $temperatureSwitcherForm['code'];

		if(!$this->temperatureCode)
		{
			if(!$session['temperatureCode'])
				$session['temperatureCode'] = self::TEMPERATURE_CODE_DEFAULT;
			$this->temperatureCode = $session['temperatureCode'];
		}

		$this->temperature = Temperature::find()->where(['code' => $this->temperatureCode])->one();

		$temperatures = Temperature::find()->all();

		foreach($temperatures as $temperature)
		{
			$this->temperatures[$temperature->code] = $temperature;
		}

		return parent::beforeAction($action);
	}

	/**
	 * ERRORS
	 */
	public function actionError()
	{
		$exception = Yii::$app->errorHandler->exception;

		if($exception !== null)
		{
			$error = [
				'message' => 'Приносим свои извинения, произошла ошибка на сервере. <br>Мы уже знаем о проблеме и вскоре ее решим. А пока вы можете попробовать еще раз, либо связаться с администрацией сайта. <br>Спасибо за понимание.',
				'code'    => isset($exception->statusCode) ? $exception->statusCode : 'Ошибка'
			];

			$this->layout = 'error';
			if(Yii::$app->request->isAjax)
			{
				echo $error['message'];
			}
			else
			{
				switch($error['code'])
				{
					case 404:
						return $this->render('/error/error404', $error);
						break;
					case 403:
						return $this->render('/error/error403', $error);
						break;
					default:
						Yii::error(Debug::vars($exception));
						return $this->render('/error/error', $error);
						break;
				}
			}
		}
	}

	/*
	 * Навигация меню в header
	 */
	public function getTopMenu()
	{
		$nav = [
			[
				'label'  => 'Об игре',
				'url'    => Yii::$app->urlManager->createUrl('/'),
				'active' => in_array(Yii::$app->controller->action->id, ['index']) ? true : false,
			],
			[
				'label'  => 'Игра',
				'url'    => Yii::$app->urlManager->createUrl('/game'),
				'active' => in_array(Yii::$app->controller->action->id, ['game']) ? true : false,
			],
			[
				'label'  => 'История игр',
				'url'    => Yii::$app->urlManager->createUrl('/history'),
				'active' => in_array(Yii::$app->controller->action->id, ['history']) ? true : false,
			]
		];

		return $nav;
	}
}
