<?php

namespace app\controllers;

use app\helpers\Debug;
use app\models\Answers;
use app\models\Cities;
use app\models\Games;
use Yii;

class MainController extends \app\controllers\FrontendController
{
	/*
	 * ОБ ИГРЕ
	 */
    public function actionIndex()
    {
		$data = [];
		
        return $this->render('index', $data);
    }

    /*
     * ИГРА
     */
    public function actionGame()
	{
		$game = NULL;
		$cities = NULL;
		$gameInfo = NULL;
		//проверяем наличие игры в сессии
		$session = Yii::$app->session;
		//unset($session['gameId']);
		if(isset($session['gameId']))
		{
			$gameId = $session['gameId'];
		}

		if(isset($gameId))
		{
			$game = Games::find()->where(['id' => $gameId])->one();
			//2 города из бд истории игры последнего шага
			$gameInfo = Answers::find()->where(['game_id' => $game->id, 'step' => $game->currentStep])->one();
			$cities = Cities::find()->where(['in', 'id', [$gameInfo->city_first_id, $gameInfo->city_second_id]])->all();
		}

		$data = [
			'game'     => $game,
			'cities'   => $cities,
			'gameInfo' => $gameInfo
		];

		return $this->render('game', $data);
	}

	/*
	 * История
	 */
	public function actionHistory()
	{
		//игры последние первыми
		$games = Games::find()->where(['status' => 'over'])->orderBy(['id' => SORT_DESC])->all();

		$data = [
			'games' => $games
		];

		return $this->render('history', $data);
	}

	/*
	 * подробная история ответов по конкрутной игре
	 */
	public function actionMore($id)
	{
		$answers = Answers::find()->where(['game_id' => $id])->orderBy(['id' => SORT_ASC])->all();

		$data = [
			'answers' => $answers
		];

		return $this->render('more', $data);
	}

	/*
	 * AJAX создание новой игры
	 */
	public function actionGameCreate()
	{
		$session = Yii::$app->session;
		$game = new Games();
		$game->create = date('Y-m-d H:h');
		$game->currentStep = 1;
		$game->status = 'on';

		if($game->save())
		{
			//пишем id игры в текущую сессию
			$session['gameId'] = $game->id;

			//2 рандомных города с разными значениями поля температуры
			$cities = Cities::find()->orderBy('RAND()')->limit(2)->groupBy('celsius')->all();

			//сохраняем данные игры (чтобы не перекликивали перезагрузкой страницы)
			$gameInfo = new Answers();
			$gameInfo->game_id = $game->id;
			$gameInfo->city_first_id = $cities[0]->id;
			$gameInfo->city_second_id = $cities[1]->id;
			$gameInfo->step = $game->currentStep;
			if($gameInfo->save())
			{
				//возвращаем положительный результат и html блок с 1м шагом игры
				$data = [
					'game'     => $game,
					'cities'   => $cities,
					'gameInfo' => $gameInfo
				];

				$result = true;
				$html = $this->renderPartial('/chunks/game_step', $data, true);
			}
			else
			{
				$result = $gameInfo->errors;
				$html = '';
			}
		}
		else
		{
			$result = $game->errors;
			$html = '';
		}

		return json_encode(['result' => $result, 'html' => $html, 'gameid' => $game->id]);
	}

	/*
	 * AJAX валидация ответа
	 */
	public function actionGameValidate()
	{
		$answer = false;
		$result = false;
		$request = Yii::$app->request->post();
		$selectedСityId = $request['cityId'];
		$gameId = $request['gameId'];

		$game = Games::find()->where(['id' => $gameId])->one();
		//info
		$gameInfo = Answers::find()->where(['game_id' => $game->id, 'step' => $game->currentStep])->one();
		//выбранный город
		$selectedСity = Cities::find()->where(['id' => $selectedСityId])->one();

		//id не выбранного города
		if($selectedСityId == $gameInfo->city_first_id)
			$noSelectCityId = $gameInfo->city_second_id;
		else
			$noSelectCityId = $gameInfo->city_first_id;

		$noSelectedСity = Cities::find()->where(['id' => $noSelectCityId])->one();

		//проверяем правильность ответа
		if($selectedСity->celsius > $noSelectedСity->celsius)
			$answer = true;

		//обновляем значения у информации по игре
		$gameInfo->city_answer_id = $selectedСity->id; //город, выбранный как правильный ответ
		if($answer)
			$gameInfo->responseStatus = 1;
		else
			$gameInfo->responseStatus = 0;

		if(!$gameInfo->save())
			$result = $gameInfo->errors;
		else
			$result = true;

		//кол-во шагов
		$steps = $gameInfo->quantitySteps();

		if($steps == 10)
		{
			//переводим игру в статус завершена
			$game->status = 'over';
			$game->save();
		}

		//возвращаем ответ пользователю и кол-во баллов
		$points = $gameInfo->quantityPoints();
		$message = \app\helpers\Useful::stepMessage($gameInfo);

		return json_encode(['points' => $points, 'message' => $message, 'steps' => $steps]);
	}

	/*
	 * AJAX переход на следующий шаг
	 * ToDo объеденить с actionGameCreate с параметром (при рефакторинге)
	 */
	public function actionGameStep()
	{
		$html = '';
		$request = Yii::$app->request->post();
		$game = Games::find()->where(['id' => $request['gameId']])->one();
		$step = $game->currentStep + 1;
		//увеличиваем шаг игры на 1
		$game->currentStep = $step;
		if($game->save())
		{
			//2 рандомных города с разными значениями поля температуры
			$cities = Cities::find()->orderBy('RAND()')->limit(2)->groupBy('celsius')->all();

			//сохраняем данные игры (чтобы не перекликивали перезагрузкой страницы)
			$gameInfo = new Answers();
			$gameInfo->game_id = $game->id;
			$gameInfo->city_first_id = $cities[0]->id;
			$gameInfo->city_second_id = $cities[1]->id;
			$gameInfo->step = $game->currentStep;
			if($gameInfo->save())
			{
				//возвращаем положительный результат и html блок с 1м шагом игры
				$data = [
					'game'     => $game,
					'cities'   => $cities,
					'gameInfo' => $gameInfo
				];

				$result = true;
				$html = $this->renderPartial('/chunks/game_step', $data, true);
			}
			else
			{
				$result = $gameInfo->errors;
				$html = '';
			}
		}
		else
		{
			$result = $game->errors;
			$html = '';
		}

		return json_encode(['result' => $result, 'html' => $html]);
	}

	/*
	 * ЗАВЕРШЕНИЕ ИГРЫ
	 */
	public function actionGameCompleat()
	{
		$session = Yii::$app->session;
		//удаляем из сессии оконченную игру
		unset($session['gameId']);
		$result = true;

		$html = '<a href="#" class="btn btn-primary js--game-start">Начать игру</a>';

		return json_encode(['result' => $result, 'html' => $html]);
	}
}
