var APP = {
	// Data
	D: {
		csrfParam: $('meta[name="csrf-param"]').attr("content"),
		csrfToken: $('meta[name="csrf-token"]').attr("content")
	},
	// Views
	V: {},
	// Extensions
	E: {},
	// Utils
	U: {}
};

APP.V.GameCreate = {
	game: '.game-block',

	init: function () {
		var _this = this,
			params = {};

		//нажатие на кнопку создания игры
		$(_this.game).find('.js--game-start').unbind('click').on('click', function (e) {

			//создаем новую игру
			$.ajax({
				url: '/game-create',
				global: false,
				type: 'POST',
				data: params,
				dataType: 'json',
				success: function (json) {
					//пишем id созданной игры в дату
					$('.js--game-block-action').attr('data-game-id', json.gameid);
					//меняем блок с городами
					$('.js--game-block-action').html(json.html);
					//прячем кнопку
					$('.js--next-btn-wrap').addClass('hidden');
					//переиничиваем game
					APP.V.Game.init();
				},
				error: function (request, status, error) {
					console.log('Ошибка: ' + request.responseText);
				}
			});

			e.preventDefault();
		});
	}
};

APP.V.Game = {
	game: '.game-block',

	init: function () {
		var _this = this,
			params = {};

		//перебираем радиокнопки и слушаем нажатие
		$(_this.game).find('.js--radio_city').each(function () {
			var $item = $(this);

			$item.unbind('change').on('change', function () {
				var cityId = $(this).find('input[name="city"]').val(),
					gameId = $(_this.game).find('.js--game-block-action').data('game-id');

				//блокируем радиокнопки
				$(_this.game).find('.js--radio_city').addClass('disabled');

				params['cityId'] = cityId;
				params['gameId'] = gameId;

				$.ajax({
					url: '/game-validate',
					global: false,
					type: 'POST',
					data: params,
					dataType: 'json',
					success: function (json) {
						//Выводим сообщение пользователю
						$('.js--message-block').html(json.message);
						//очки
						$('.js--points-quantity').text(json.points);

						//если уже 10 ходов - выводим сообщение, что игра окончена
						if(parseInt(json.steps) == 10){
							//удаляем блок с городами
							$('.js--game-cities-wrap').remove();
							$('.js--game-step-wrap').append('<div class="game-compleat">Игра окончена</div>');
							//отображаем кнопку ОК
							$('.js--compleat-btn-wrap').removeClass('hidden');
						}else{
							//показываем кнопку перехода на след вопрос
							$('.js--next-btn-wrap').removeClass('hidden');
						}
					},
					error: function (request, status, error) {
						console.log('Ошибка: ' + request.responseText);
					}
				});
			})
		});

		//нажатие кнопки "Следующие города"
		$('.js--next-btn').unbind('click').on('click', function(e){
			var gameId = $(_this.game).find('.js--game-block-action').data('game-id');
			params['gameId'] = gameId;

			//запрос на новую пару городов
			$.ajax({
				url: '/game-step',
				global: false,
				type: 'POST',
				data: params,
				dataType: 'json',
				success: function (json) {
					//меняем блок с городами
					$('.js--game-block-action').html(json.html);
					//прячем кнопку
					$('.js--next-btn-wrap').addClass('hidden');
					//переиничиваем game
					APP.V.Game.init();
				},
				error: function (request, status, error) {
					console.log('Ошибка: ' + request.responseText);
				}
			});

			e.preventDefault();
		});

		//Нажатие на согласие о завершении игры
		$('.js--compleat-btn').unbind('click').on('click', function(e){

			//удаление сессии, возврат на первоначальное меню
			$.ajax({
				url: '/game-compleat',
				global: false,
				type: 'POST',
				data: params,
				dataType: 'json',
				success: function (json) {
					//зануляем data атрибут игры
					$('.js--game-block-action').attr('data-game-id', 'new');
					//показываем ссылку начать игру
					$('.js--game-block-action').html(json.html);
					//скрываем кнопку ОК
					$('.js--compleat-btn-wrap').addClass('hidden');
					//обнуляем очки
					$('.js--points-quantity').text('0');
					//переиничиваем game
					APP.V.Game.init();
					APP.V.GameCreate.init();
				},
				error: function (request, status, error) {
					console.log('Ошибка: ' + request.responseText);
				}
			});

			e.preventDefault();
		});

	}
};

APP.init = function () {
	//Создание игры
	APP.V.GameCreate.init();
	//Игра
	APP.V.Game.init();
};

/**************
 *    RUN     *
 **************/

(function($) {
	$(document).ready(function() {
		APP.init();
	});
})(jQuery);