<?php

// Тестовый сервер
use yii\web\Application;

if (in_array($_SERVER['HTTP_HOST'], [
	'talantest',
	'localhost'
]))
{
	defined('YII_ENV') or define('YII_ENV', 'dev');
	defined('YII_DEBUG') or define('YII_DEBUG', true);
}
else
{
	defined('YII_ENV') or define('YII_ENV', 'prod');
	defined('YII_DEBUG') or define('YII_DEBUG', false);
};

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
