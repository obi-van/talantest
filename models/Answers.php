<?php

namespace app\models;
use app\helpers\Debug;
use Yii;

/**
 * This is the model class for table "tal_answers".
 *
 * @property string $id
 * @property string $game_id игра
 * @property string $city_first_id первый город
 * @property string $city_second_id второй город
 * @property string $city_answer_id город ответ
 * @property int $responseStatus правильно/не правильно
 *
 * @property Cities $cityFirst
 * @property Cities $citySecond
 * @property Cities $cityAnswer
 * @property Games $game
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%answers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'city_first_id', 'city_second_id', 'city_answer_id', 'responseStatus'], 'integer'],
            [['city_first_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_first_id' => 'id']],
            [['city_second_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_second_id' => 'id']],
            [['city_answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_answer_id' => 'id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
			[['step'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'city_first_id' => 'City First ID',
            'city_second_id' => 'City Second ID',
            'city_answer_id' => 'City Answer ID',
            'responseStatus' => 'Response Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityFirst()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_first_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitySecond()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_second_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityAnswer()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    //кол-во очков (правильных ответов в игре)
	public function quantityPoints()
	{
		$quantity = 0;
		//считаем кол-во правильных ответов
		$answers = Answers::find()->where(['game_id' => $this->game_id, 'responseStatus' => 1])->all();

		$quantity = count($answers);

		return $quantity;
	}

	//кол-во шагов (вопросов)
	public function quantitySteps()
	{
		$game = Games::find()->where(['id' => $this->game_id])->one();
		$steps = $game->currentStep;
		return $steps;
	}
}
