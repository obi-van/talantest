<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tal_games".
 *
 * @property string $id
 * @property string $create Дата создания игры (начала)
 * @property int $currentStep Текущий шаг (всего 10)
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%games}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create', 'status'], 'safe'],
            [['currentStep'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create' => 'Create',
            'currentStep' => 'Current Step',
        ];
    }
}
