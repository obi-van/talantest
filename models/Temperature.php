<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tal_temperature".
 *
 * @property string $id
 * @property string $code
 * @property string $sign
 */
class Temperature extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%temperature}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string', 'max' => 10],
            [['sign'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'sign' => 'Знак',
        ];
    }
}
