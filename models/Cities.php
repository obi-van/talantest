<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tal_cities".
 *
 * @property string $id
 * @property string $title
 * @property string $country_id
 * @property string $celsius Абсолютный максимум
 *
 * @property Countries $country
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['country_id'], 'integer'],
            [['celsius'], 'number'],
            [['title'], 'string', 'max' => 50],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'country_id' => 'Country ID',
            'celsius' => 'Celsius',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }
}
