<div class="temperature affix-temperature">
	<form method="post" class="form-temperature-switcher" role="form" enctype="multipart/form-data" autocomplete="off">
		<select class="select select_styl select_black" name="TemperatureSwitcherForm[code]" onchange="this.form.submit()">
			<? foreach($temperatures as $temperature): ?>
				<option class="select__item" <? if($userTemperatureCode === $temperature->code): ?>selected="selected"<? endif ?> value="<?= $temperature->code ?>">
					<?= $temperature->sign ?>
				</option>
			<? endforeach ?>
		</select>
	</form>
</div>