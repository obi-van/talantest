<?php

namespace app\widgets;
use app\models\Temperature;
use Yii;
use yii\base\Widget;
use app\helpers\Debug;

class TemperatureSwitcher extends Widget
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$session = Yii::$app->session;

		$temperatures = Temperature::find()->orderby(['id'=>SORT_ASC])->all();
		$userTemperatureCode = $session['temperatureCode'];

		$data = [
			'temperatures'        => $temperatures,
			'userTemperatureCode' => $userTemperatureCode
		];

		return $this->render('temperature', $data);
	}
}