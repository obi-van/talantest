<?php


namespace app\helpers;
use app\helpers\Debug;
use app\models\Answers;
use Yii;

class Useful
{
    //кол-во баллов при существующей игре
	static function quantityPoints($game)
	{
		$quantity = 0;
		if($game)
		{
			//считаем кол-во правильных ответов
			$answers = Answers::find()->where(['game_id' => $game->id, 'responseStatus' => 1])->all();
			$quantity = count($answers);
		}

		return $quantity;
	}

	//сообщение пользователю
	static function stepMessage($gameInfo)
	{
		$message = '';

		if(!empty($gameInfo) && !empty($gameInfo->city_answer_id))
		{
			if($gameInfo->responseStatus == 1)
			{
				$message = '<div class="message-successfull">Поздравляем! Это правильный ответ! Температура в '
					. $gameInfo->cityFirst->title . ': '. \app\helpers\Useful::convertToTemperature($gameInfo->cityFirst->celsius) . ', а температура в ' . $gameInfo->citySecond->title
					. ': ' . \app\helpers\Useful::convertToTemperature($gameInfo->citySecond->celsius) . ' </div>';
			}
			else
			{
				$message = '<div class="message-error">Жаль! Температура в ' . $gameInfo->cityFirst->title . ': '
					. \app\helpers\Useful::convertToTemperature($gameInfo->cityFirst->celsius) .', а температура в ' . $gameInfo->citySecond->title . ': '
					. \app\helpers\Useful::convertToTemperature($gameInfo->citySecond->celsius) . ' Повезет со следующими городами!</div>';
			}
		}

		return $message;
	}

	//конвертер температур
	static function convertToTemperature($celsius)
	{
		$temperature = Yii::$app->controller->temperature;

		$val = $celsius . ' ' . $temperature->sign;

		if($temperature->code == 'FARENHEIT')
		{
			//пересчитываем C в F
			$val = ($celsius * 1.8) + 32;
			$val = number_format($val, 2, '.', '') . ' ' . $temperature->sign;
		}

		return $val;
	}
}