<?php

/**
 * Параметры подключения к БД
 */

function setDbOptions($docroot)
{
	// Базовые установки соединения
	$settings = [
		'class' => 'yii\db\Connection',
		'charset' => 'utf8',
		'tablePrefix' => 'tal_',
	];

	//боевой для публикации
	if(YII_ENV_PROD)
	{
		return $settings + [
				'dsn' => 'mysql:host=shared-28.smartape.ru;dbname=user2906_talan_test',
				'username' => 'talan',
				'password' => 'X2l8U7n2',
			];
	}
	else
	{
		return $settings + [
				'dsn' => 'mysql:host=localhost;dbname=talan_test',
				'username' => 'root',
				'password' => '',
			];
	}
}